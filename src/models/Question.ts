import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Questionnaire } from "./Questionnaire";

@Entity({name: 'questions'})
export class Question {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    text: string;

    @Column()
    maxScore: number;

    @Column({default: 0})
    sortOrder: number;

    @Column()
    questionnaireId: number;

    @ManyToOne(type => Questionnaire, quesionnaire => quesionnaire.id)
    questionnaire: Questionnaire;
}