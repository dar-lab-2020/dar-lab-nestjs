import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Question } from 'src/models/Question';

@Injectable()
export class QuestionService {

    constructor(
        @InjectRepository(Question)
        private readonly questionRepository: Repository<Question>
    ) {}

    getAll(criteria = {}) {
        return this.questionRepository.find(criteria);
    }

    create(question: Question) {
        return this.questionRepository.save(question);
    }

    update(id: number, data: Partial<Question>) {
        return this.questionRepository.update(id, data);
    } 

    delete(id: number) {
        return this.questionRepository.delete(id);
    }

    getById(id: number) {
        return this.questionRepository.findOne(id);
    }

}