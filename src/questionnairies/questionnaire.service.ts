import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Questionnaire } from "../models/Questionnaire";
import { Repository } from "typeorm";

@Injectable()
export class QuestionnaireService {

    constructor(
        @InjectRepository(Questionnaire)
        private readonly questionnaireRepository: Repository<Questionnaire>
    ) {}

    getAll(criteria = {}) {
        return this.questionnaireRepository.find(criteria);
    }

    create(questionnaire: Questionnaire) {
        return this.questionnaireRepository.save(questionnaire);
    }

    update(id: number, data: Partial<Questionnaire>) {
        return this.questionnaireRepository.update(id, data);
    } 

    delete(id: number) {
        return this.questionnaireRepository.delete(id);
    }

    getById(id: number) {
        return this.questionnaireRepository.findOne(id);
    }

}