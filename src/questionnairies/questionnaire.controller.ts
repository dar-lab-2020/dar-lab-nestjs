import { Controller, Body, Post, Get, Put, Param, Delete } from "@nestjs/common";
import { QuestionnaireService } from "./questionnaire.service";

@Controller('questionnairies')
export class QuestionnaireController {

    constructor(private readonly questionnairyService: QuestionnaireService) {}

    @Get()
    getAll() {
        return this.questionnairyService.getAll();
    }

    @Get(':id')
    getById(@Param('id') id: number) {
        return this.questionnairyService.getById(id);
    }

    @Post()
    create(@Body() questionnaire) {
        return this.questionnairyService.create(questionnaire);
    }

    @Put(':id')
    edit(@Param('id') id: number, @Body() data) {
        return this.questionnairyService.update(id, data);
    }

    @Delete(':id')
    delete(@Param('id') id: number) {
        return this.questionnairyService.delete(id);
    }
}