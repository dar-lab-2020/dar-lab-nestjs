import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from '../models/Category';
import { Questionnaire } from 'src/models/Questionnaire';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import { QuestionnaireService } from './questionnaire.service';
import { QuestionnaireController } from './questionnaire.controller';
import { Question } from 'src/models/Question';
import { QuestionService } from './question.service';
import { QuestionController } from './question.controller';
import { UploaderController } from './uploader.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([Category, Questionnaire, Question])
    ],
    exports: [TypeOrmModule],
    controllers: [
        CategoryController,
        QuestionnaireController,
        QuestionController,
        UploaderController,
    ],
    providers: [
        CategoryService,
        QuestionnaireService,
        QuestionService,
    ],
})
export class QuestionnairiesModule {

}