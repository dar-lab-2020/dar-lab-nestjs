import { Controller, Body, Post, Get, Put, Param, Delete, Query } from "@nestjs/common";
import { QuestionService } from './question.service';

@Controller('questions')
export class QuestionController {

    constructor(private readonly questionService: QuestionService) {}

    @Get()
    getAll(@Query() query) {
        const criteria = {};

        if (query.questionnaireId) {
            criteria['questionnaireId'] = query.questionnaireId
        }

        return this.questionService.getAll(criteria);
    }

    @Get(':id')
    getById(@Param('id') id: number) {
        return this.questionService.getById(id);
    }

    @Post()
    create(@Body() question) {
        return this.questionService.create(question);
    }

    @Put(':id')
    edit(@Param('id') id: number, @Body() data) {
        return this.questionService.update(id, data);
    }

    @Delete(':id')
    delete(@Param('id') id: number) {
        return this.questionService.delete(id);
    }
}